-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema Hotels
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Hotels
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Hotels` DEFAULT CHARACTER SET utf8 ;
-- -----------------------------------------------------
-- Schema hotelssystem
-- -----------------------------------------------------
USE `Hotels` ;

-- -----------------------------------------------------
-- Table `Hotels`.`Client`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotels`.`Client` (
  `idClients` INT NOT NULL,
  `clientName` VARCHAR(45) NULL,
  `clientSureName` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `bankAccount` VARCHAR(45) NULL,
  `clientState` TINYINT NULL,
  PRIMARY KEY (`idClients`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Hotels`.`Hotels`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotels`.`Hotels` (
  `idHotels` INT NOT NULL,
  `hotelName` VARCHAR(45) NULL,
  `hotelAddress` VARCHAR(45) NULL,
  `hotelsRooms` VARCHAR(45) NULL,
  `hotelsFreeRoom` VARCHAR(45) NULL,
  PRIMARY KEY (`idHotels`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Hotels`.`Rooms`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotels`.`Rooms` (
  `idRoom` INT NOT NULL,
  `roomState` VARCHAR(45) NULL,
  `roomPrice` DECIMAL NULL,
  `roomInDate` VARCHAR(45) NULL,
  `roomOutDate` DATETIME NULL,
  `Hotels_idHotels` INT NOT NULL,
  PRIMARY KEY (`idRoom`, `Hotels_idHotels`),
  INDEX `fk_Rooms_Hotels1_idx` (`Hotels_idHotels` ASC),
  CONSTRAINT `fk_Rooms_Hotels1`
    FOREIGN KEY (`Hotels_idHotels`)
    REFERENCES `Hotels`.`Hotels` (`idHotels`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Hotels`.`Payments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotels`.`Payments` (
  `idPayments` INT NOT NULL,
  `paymentDate` DATETIME NULL,
  `paymentSum` DOUBLE NULL,
  `bankAccount` INT NULL,
  `paymentState` VARCHAR(45) NULL,
  `Clients_idClients` INT NOT NULL,
  PRIMARY KEY (`idPayments`, `Clients_idClients`),
  INDEX `fk_Payments_Clients1_idx` (`Clients_idClients` ASC),
  CONSTRAINT `fk_Payments_Clients1`
    FOREIGN KEY (`Clients_idClients`)
    REFERENCES `Hotels`.`Client` (`idClients`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Hotels`.`Booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotels`.`Booking` (
  `idBooking` INT NOT NULL AUTO_INCREMENT,
  `hotelId` INT NOT NULL,
  `bookingInDate` DATETIME NULL,
  `bookingOutDate` DATETIME NULL,
  `roomID` VARCHAR(45) NOT NULL,
  `clientName` VARCHAR(45) NULL,
  `clientSureName` VARCHAR(45) NULL,
  `Payments_idPayments` INT NOT NULL,
  PRIMARY KEY (`idBooking`, `hotelId`, `roomID`, `Payments_idPayments`),
  UNIQUE INDEX `idBooking_UNIQUE` (`idBooking` ASC),
  INDEX `fk_Booking_Payments1_idx` (`Payments_idPayments` ASC),
  CONSTRAINT `fk_Booking_Payments1`
    FOREIGN KEY (`Payments_idPayments`)
    REFERENCES `Hotels`.`Payments` (`idPayments`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Hotels`.`Reviews`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotels`.`Reviews` (
  `idReviews` INT NOT NULL,
  `clientReviews` VARCHAR(45) NULL,
  `starsReviews` VARCHAR(45) NULL,
  `dateReview` VARCHAR(45) NULL,
  `Hotels_idHotels` INT NOT NULL,
  PRIMARY KEY (`idReviews`, `Hotels_idHotels`),
  INDEX `fk_Reviews_Hotels1_idx` (`Hotels_idHotels` ASC),
  CONSTRAINT `fk_Reviews_Hotels1`
    FOREIGN KEY (`Hotels_idHotels`)
    REFERENCES `Hotels`.`Hotels` (`idHotels`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Hotels`.`Clients_has_Booking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotels`.`Clients_has_Booking` (
  `Clients_idClients` INT NOT NULL,
  `Booking_idBooking` INT NOT NULL,
  `Booking_hotelId` INT NOT NULL,
  PRIMARY KEY (`Clients_idClients`, `Booking_idBooking`, `Booking_hotelId`),
  INDEX `fk_Clients_has_Booking_Booking1_idx` (`Booking_idBooking` ASC, `Booking_hotelId` ASC),
  INDEX `fk_Clients_has_Booking_Clients1_idx` (`Clients_idClients` ASC),
  CONSTRAINT `fk_Clients_has_Booking_Clients1`
    FOREIGN KEY (`Clients_idClients`)
    REFERENCES `Hotels`.`Client` (`idClients`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Clients_has_Booking_Booking1`
    FOREIGN KEY (`Booking_idBooking` , `Booking_hotelId`)
    REFERENCES `Hotels`.`Booking` (`idBooking` , `hotelId`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Hotels`.`Booking_has_Rooms`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Hotels`.`Booking_has_Rooms` (
  `Booking_idBooking` INT NOT NULL,
  `Booking_hotelId` INT NOT NULL,
  `Booking_roomID` VARCHAR(45) NOT NULL,
  `Rooms_idRoom` INT NOT NULL,
  `Rooms_Hotels_idHotels` INT NOT NULL,
  PRIMARY KEY (`Booking_idBooking`, `Booking_hotelId`, `Booking_roomID`, `Rooms_idRoom`, `Rooms_Hotels_idHotels`),
  INDEX `fk_Booking_has_Rooms_Rooms1_idx` (`Rooms_idRoom` ASC, `Rooms_Hotels_idHotels` ASC),
  INDEX `fk_Booking_has_Rooms_Booking1_idx` (`Booking_idBooking` ASC, `Booking_hotelId` ASC, `Booking_roomID` ASC),
  CONSTRAINT `fk_Booking_has_Rooms_Booking1`
    FOREIGN KEY (`Booking_idBooking` , `Booking_hotelId` , `Booking_roomID`)
    REFERENCES `Hotels`.`Booking` (`idBooking` , `hotelId` , `roomID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Booking_has_Rooms_Rooms1`
    FOREIGN KEY (`Rooms_idRoom` , `Rooms_Hotels_idHotels`)
    REFERENCES `Hotels`.`Rooms` (`idRoom` , `Hotels_idHotels`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
