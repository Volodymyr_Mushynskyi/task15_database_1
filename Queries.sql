use labor_sql;

-- 1
select model,price from laptop
order by price;

select model,ram,speed,price from laptop
 where price > 1000 
 order by ram ASC, price DESC;
 
 select * from printer
 where color = "y"
 order by price desc;
 
 select model,speed,hd,cd,price from pc
 where price<600 and (cd="12x" or cd="24x")
 order by speed desc;
 
 select name, class from Ships
 order by name;
 
 select * from pc
 where speed>=500 and price<800
 order by price;
 
 select * from printer
 where type!="Matrix" and price<300
 order by type desc;
 
 select model,speed from pc
 where price between 400 and 600
 order by hd;
 
 select model,speed,hd from pc
 where (hd=10 or hd=20) and  model="1232"
 order by speed;
 
 select model,speed,hd,price from laptop
 where screen>=12
 order by price desc;
 
 select model,type,price from printer
 where price<300
 order by type desc;
 
 select model,ram,price from laptop
 where ram=64
 order by screen asc;
 
 select model,ram,price from pc
 where ram>64
 order by hd asc;
 
 select model,speed,price from pc
 where speed between 500 and 750
 order by hd desc;
 
 select * from outcome_o
 where outcome_o.out>2000
 order by date desc;
 
 select * from income_o
 where inc between 5000 and 10000
 order by inc;
 
 select * from income
 where point=1 
 order by inc asc;
 
 select * from outcome
 where point=2 
 order by outcome.out asc;
 
 select * from classes
 where country="Japan"
 order by type desc;
 
 select name,launched from ships
 where launched between 1920 and 1942
 order by launched desc;
 
 select ship,battle,result from outcomes
 where battle="Guadalcanal" and (result="OK" or result="damaged")
 order by ship desc;

 select ship,battle,result from outcomes
 where result="sunk"
 order by ship desc;
 
 select class, displacement from classes
 where displacement>=40
 order by type asc;
 
 select trip_no,town_from,town_to from trip
 where trip.town_from="London" or trip.town_to="London"
 order by time_out;
 
 select trip_no,town_from,town_to from trip
 where plane="TU-134"
 order by time_out;
 
 select trip_no,town_from,town_to from trip
 where plane="IL-86"
 order by time_out;
 
 select trip_no,town_from,town_to from trip
 where town_to!="Rostov"
 order by time_out;
 
 -- 2
 select * from pc
 where model rlike '[1]{2}';
 
 select * from outcome
 where date rlike '^([0-9]{4}\-{1}(03){1}\-{1}[0-9]{2})$';
 
 select * from ships
 where name rlike '^W' and 'n$';
 
 select name,launched from ships
 where name rlike '.*[^a]$';
 
 select name from battles
 where name rlike '[^a-zA-Z][:space:].*[^c]$';
 
 select * from trip
 where hour(time_out)>=12 and hour(time_out)<=17;
 
 select * from trip
 where hour(time_in)>=12 and hour(time_in)<=17;
 
 select date from pass_in_trip
 where place rlike '^[1]';
 
 select date from pass_in_trip
 where place rlike 'c$';
 
 select name from Passenger
 where name rlike '[^a-zA-Z][:space:].*[^J]$';
 
 select name from Passenger
 where name rlike '[^a-zA-Z][:space:]^[C]';
 
 
 -- 3
 select maker,type,speed,hd from pc join product on pc.model=product.model
 where hd<=8;
 
 select maker,type,speed from pc join product on pc.model=product.model
 where speed>=600;
 
 select maker,type,speed from laptop join product on laptop.model=product.model
 where speed<=500;

 select distinct a.model, b.model, a.hd, a.ram from laptop a join laptop b on a.hd=b.hd
 where a.hd=b.hd and a.ram=b.ram and a.code <> b.code and a.model>b.model ; 
 
 select country,type from classes
 where type="bb" or type="bc";
 
 select b.model,maker,price from pc a join product b on a.model=b.model
 where a.price>=600;
 
 select b.model,maker,price from printer a join product b on a.model=b.model
 where a.price>300;
 
 select maker,b.model,price from pc a join product b on a.model=b.model;
 
 select maker,a.model,price from pc a join product b on a.model=b.model;

 select maker,type,a.model,speed from laptop a join product b on a.model=b.model
 where speed>=600;
 
 select a.name, b.displacement from ships a join classes b on a.class=b.class; 
 
 select a.ship, b.name, b.date from outcomes a join battles b on a.battle=b.name
 where a.result="OK" or a.result="damaged";
 
 select a.name, b.country from ships a join classes b on a.class=b.class;
 
 select a.plane, b.name from trip a join company b on a.ID_comp=b.ID_comp;
 
 select a.plane, b.name from trip a join company b on a.ID_comp=b.ID_comp
 where a.plane="Boeing";
 
 select a.name, b.date from passenger a join pass_in_trip b on a.ID_psg=b.ID_psg
 
 
 
 
 